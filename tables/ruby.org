#+TITLE: Ruby Data Structures Table

| data structure | instantiate     | literal         | count               | get                            | add                        | visit                         | test membership | remove                              |
|----------------+-----------------+-----------------+---------------------+--------------------------------+----------------------------+-------------------------------+-----------------+-------------------------------------|
| (enumerable)   | n/a (abstract)  | n/a             | ~#count~, ~#length~ | ~#find(block)~, ~#take~        | n/a                        | ~#each~                       | ~#member?~      | n/a                                 |
| hash-map       | ~Hash.new~      | ~{ key: val }~  | enumerable          | ~#fetch~, ~#dig~, ~#slice~     | ~#store~, ~[key]=val~      | enumerable, ~#each_key~       | enumerable      | ~#shift~, ~#delete~, ~#delete_if~   |
| vector         | ~Array.new~     | ~[, ]~          | enumerable          | ~#fetch~, ~#dig~, ~#slice~     | ~#push~, ~#unshift~        | enumerable, ~#each_index~     | enumerable      | ~#shift~, ~#pop~, ~#delete_{at,if}~ |
| string         | ~String.new~    | "content"       | ~#length~           | ~#bytes~, ~#getbyte~, ~#slice~ | ~#concat~, ~#<<~           | ~#each_{byte,char}~, ~#split~ | ~#include?~     | ~#chop~, ~#strip~                   |
| set*           | ~Set.new~       | ~Set[, ]~       | enumerable          | enumerable                     | ~#add~, ~#union~, ~#merge~ | enumerable                    | ~&~, ~<~        | ~#delete~, ~#difference~            |
| sorted-set*    | ~SortedSet.new~ | ~SortedSet[, ]~ | enumerable          | enumerable                     | set                        | enumerable                    | set             | set                                 |
| queue          | use vector      | ~[, ]~          | enumerable          | ~#first~                       | ~#push~                    | enumerable                    | enumerable      | ~#pop~                              |
| ring-buffer**  |                 |                 |                     |                                |                            |                               |                 |                                     |

 * use ~require 'set'~
 ** implement your own lmao
